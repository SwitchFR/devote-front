var index_vote = 0;
$(document).ready(function(){
    $("#nav_placeholder").load("navbar.html",function(){
        $('.retro_btn').prepend('<div class="hover"><span></span><span></span><span></span><span></span><span></span></div>');
        $(".link_"+$("body").attr("id")).addClass("active").find("a").attr("href","#");

        if(Cookies.get("token") != undefined)
        {
            $("#login").attr("href","account.html").removeAttr("data-toggle data-target").after('<a href="#" class="retro_btn red" id="logout"><div class="hover"><span></span><span></span><span></span><span></span><span></span></div><p class="mb-0">Déconnexion</p></a>').find("p").text("Mon compte");
        }
    });

    $("body").on("click","#signup",function(){
        $("#btn_sign").attr("data-mode","sign_up");
        $("#pass_input").addClass("mb-4");
        $("#subtext").html('Déjà inscrit ? <a href="#" id="signin" tabindex="-1">C' + "'" + 'est par ici !</a>' )
        $(".modal-content").animate({
            height: "+=270"
        },500);
        $(".signup_only").fadeIn(750,function(){
            $("#login_input").focus();
        });
    });

    $("body").on("click","#signin",function(){
        $("#btn_sign").attr("data-mode","sign_in");
        $("#pass_input").removeClass("mb-4");
        $("#subtext").html('Pas encore inscrit ? <a href="#" id="signup" tabindex="-1">C' + "'" + 'est par ici !</a>');
        $(".modal-content").animate({
            height: "-=270"
        },750);
        $(".signup_only").fadeOut(400,function(){
            $("#login_input").focus();
        });
    });

    $("body").on("click","#logout",function(){
        Cookies.remove("token");
        location.reload();
    });

    $("body").on("focusout","#login_input",function(){
        var actu = $(this);
        var mail = $(this).val();
        if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
        {
            $(this).removeClass("is-invalid");
            if($("#btn_sign").attr("data-mode") == "sign_up")
            {
                $.get(url+"check?mail="+mail,function(data){
                    $(actu).addClass("is-valid");
                }).fail(function(data){
                    $(actu).addClass("is-invalid");
                });
            }
        }
        else
        {
            $(this).removeClass("is-valid").addClass("is-invalid");
        }
    });

    $("body").on("click","#btn_sign",function(){
        if($(this).attr("disabled") == undefined)
        {
            $(this).attr("disabled",true);
            var tab_infos = {};
            $.  each($("#connexion_modal input"),function(){
                if($(this).attr("style") == "" || $(this).attr("style") == undefined)
                {
                    if($(this).val() == "")
                    {
                        $(this).addClass("is-invalid");
                    }
                    else
                    {
                        $(this).removeClass("is-invalid");
                        var id = $(this).attr("id").split("_")[0];
                        tab_infos[id] = $(this).val();
                    }
                }
            });
    
            if($("#connexion_modal .is-invalid").length == 0)
            {
                $.post(url+"user",tab_infos,function(data){
                    $("#login").removeAttr("data-toggle data-target").after('<a href="#" class="retro_btn red" id="logout"><div class="hover"><span></span><span></span><span></span><span></span><span></span></div><p class="mb-0">Déconnexion</p></a>').find("p").text("Mon compte");
                    var infos = JSON.parse(atob(data.token));
                    $.notify({
                        message: 'Bienvenue ' + infos.prenom + " !"
                    },{
                        // settings
                        type: 'success',
                        z_index: 2000,
                        placement: {
                            from: 'bottom',
                            align: "center"
                        }
                    });
                    $(".star").addClass("star-activate");
                    $("#connexion_modal").modal("hide");
                    Cookies.set("token",data.token);
                }).fail(function(){
                    $.notify({
                        message: 'Identifiants incorrects !'
                    },{
                        // settings
                        type: 'danger',
                        z_index: 2000,
                        placement: {
                            from: 'bottom',
                            align: "center"
                        }
                    });
                    $("#btn_sign").attr("disabled",false);
                });
            }
        }
    });

    $("body").on("keydown","#pass_input",function(e){
        if(e.which == 13)
        {
            $("#btn_sign").click();
        }
    });

    $("body").on("mouseout",".nav-item",function(e){
        if(e.target == this)
        {
            $(".retro_btn").trigger('blur');
        }
    });

    $("body").on("click","input.star",function(e){
        var parent = $(this).closest(".cont");
        if(Cookies.get("token") != undefined)
        {
            if(!$(parent).find(".rev-box").hasClass("onview"))
            {
                $(parent).find(".rev-box").addClass("onview");
                $(parent).find(".rev-box").fadeIn(500);
                $(parent).animate({
                    height: "+=50"
                },500);
            }
            index_vote = $(this).attr("data-index");

            $.each($(parent).find("input.star"),function(){
                if($(this).attr("data-index") <= index_vote)
                {
                    $(this).next().addClass("clicked");
                }
                else
                {
                    $(this).next().removeClass("clicked");
                }

            });

        }
        else
        {
            e.preventDefault();
        }
    });

    $("body").on("click",".btn-vote",function(){
        var actu_cont = $(this).closest(".cont");

        $(this).attr("disabled",true);
        var vote_saved = false;
        var vote_infos = {};
        vote_infos.token = Cookies.get("token");
        vote_infos.note = index_vote;
        vote_infos.framework = $(actu_cont).attr("data-framework");
        vote_infos.text = $(actu_cont).find(".review").val();

        var notify = $.notify('<strong>Attention</strong> Nous enregistrons votre vote...', {
            allow_dismiss: false,
            showProgressbar: true,
            placement: {
                from: 'top',
                align: "center"
            }
        });

        var interval = setInterval(function(){
            notify.update({'progress': 0})
            if(vote_saved)
            {
                notify.update({'type': 'success', 'message': 'Vote enregistré !','progress': 100})
                clearInterval(interval);
            }
        },1000);

        $.post(url+"note",vote_infos,function(data){
            vote_saved = true;
        }).fail(function(data){
            notify.update({'type': 'danger', 'message': 'Une erreur est survenue !','progress': 100});
            $(actu_cont).find(".btn-vote").attr("disabled",false);
        });
    });
});