$(document).ready(function(){
    var url_framework = url+"frameworks";
    if(Cookies.get("token") != undefined)
    {
        url_framework+="?token="+Cookies.get("token");
    }
    $.get(url_framework,function(data){
        $.each(data.result,function(index){
            if(index%2 == 0 && index != 0)
            {
                $(".ligne_result").after('<div class="col-12 row ligne_result mb-3"></div>');
            }
            var libelle = "Donnez votre avis !";
            var texte = "";
            if(this.note != undefined)
            {
                libelle = "Vous avez déjà noté ce framework !";
                texte = this.note.message;
            }
            else
            {
                if(Cookies.get("token") == undefined)
                {
                    libelle = "Connectez vous pour laisser un avis !";
                }
            }
            $(".ligne_result").last().append('<div class="col-lg-12 row mt-5"><div class="col-lg-6"><img class="w-50" src="'+this.image+'"><div class="cont w-50" data-framework="'+this.id+'"><div class="stars"><h5 class="w-100 mt-3" id="review_lbl">'+libelle+'</h5><div><input class="star star-activate star-5" id="star-5-2-'+index+'" data-index="5" type="radio" name="star"><label class="star star-activate star-5" for="star-5-2-'+index+'"></label><input class="star star-activate star-4" id="star-4-2-'+index+'" data-index="4" type="radio" name="star"><label class="star star-activate star-4" for="star-4-2-'+index+'"></label><input class="star star-activate star-3" id="star-3-2-'+index+'" data-index="3" type="radio" name="star"><label class="star star-activate star-3" for="star-3-2-'+index+'"></label><input class="star star-activate star-2" id="star-2-2-'+index+'" data-index="2" type="radio" name="star"><label class="star star-activate star-2" for="star-2-2-'+index+'"></label><input class="star star-activate star-1" id="star-1-2-'+index+'" data-index="1" type="radio" name="star"><label class="star star-activate star-1" for="star-1-2-'+index+'"></label><div class="rev-box" style="display: none;"><label id="lbl-vote">Laisse nous un commentaire !</label><textarea class="review" col="30" name="review" placeholder="Aucun commentaire..."></textarea><button class="btn btn-success btn-vote"><i class="fas fa-check"></i></button></div></div></div></div></div><div class="col-lg-6"><p>'+this.description+'</p></div></div>');
            if(this.note != undefined)
            {
                $(".cont").last().find('[data-index="'+this.note.note+'"]').click();
                $(".cont").last().find('.star').attr("disabled",true);
                $(".cont").last().find('.review').val(texte).attr("readonly",true);
                $(".cont").last().find('.btn-vote').attr("disabled",true);
            }
        });
    });

    
});