$(document).ready(function(){
    if(Cookies.get("token") != undefined)
    {
        $.get(url+"user/info",{token : Cookies.get("token")},function(data){
            $("#mail_info").val(data.result.mail).attr("data-origin",data.result.mail);
            $("#fname_info").val(data.result.prenom).attr("data-origin",data.result.prenom);
            $("#name_info").val(data.result.nom).attr("data-origin",data.result.nom);

            $(".lds-ring").fadeOut(200,function(){
                $(".container-fluid").removeAttr("hidden");
            });

            if(data.notes != undefined)
            {
                $("#placeholder_note").remove();

                $.each(data.notes,function(){
                    if(this.message == "")
                    {
                        this.message = "Aucun commentaire...";
                    }
                    $("#table_note tbody").append('<tr><td>'+this.element+'</td><td>'+this.note+'</td><td>'+this.message+'</td><td>'+this.date+'</td></tr>')
                });
                $("#table_note").removeAttr("hidden")
            }
        });
    }
    else
    {
        window.location = "index.html";
    }

    $("body").on("keyup",".input_account",function(){
        $(this).closest(".container_info").find(".btn-account").attr("disabled",$(".input_info").filter(function(){return $(this).val() != $(this).attr("data-origin")}).length == 0);

        if($(this).val() != $(this).attr("data-origin"))
        {
            $(this).closest(".container_info").find(".btn-account").attr("disabled",false);
        }

        if($(this).hasClass("input_info"))
        {
            if($(this).val() == "")
            {
                $(this).addClass("is-invalid");
            }
            else
            {
                $(this).removeClass("is-invalid");
            }
            if($(this).is("#mail_info") && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(this).val()))
            {
                $(this).addClass("is-invalid");
            }
        }
    });

    $("body").on("keyup",".input_pass",function(){
        var actu_object = $(this);

        if($(".input_pass").filter(function(){return $(this).val() == $(actu_object).val();}).length == 2)
        {
            $(".input_pass").last().removeClass("is-invalid");
        }
        else
        {
            $(".input_pass").last().addClass("is-invalid");
        }
    });

    $("body").on("click","#btn-modif-info",function(){
        tab_infos = {};
        tab_infos.token = Cookies.get("token");
        if($(".input_info.is-invalid").length == 0)
        {
            var notif = $.notify({
                message: 'Envoie en cours...'
            },{
                // settings
                type: 'warning',
                z_index: 2000,
                placement: {
                    from: 'bottom',
                    align: "center"
                }
            });
            $(this).attr("disabled",true);
            $.each($(".input_info"),function(){
                var id = $(this).attr("id");
                tab_infos[id.split("_")[0]] = $(this).val();
            });
            $.ajax({
                url: url+"user/info",
                type: 'PUT',
                data: tab_infos,
                success: function(data) {

                    $.each(tab_infos,function(index){
                        $("#"+index+"_info").attr("data-origin",this.toString());
                    });

                    notif.close();
                    notif = $.notify({
                        message: 'Modification effectué !'
                    },{
                        // settings
                        type: 'success',
                        z_index: 2000,
                        placement: {
                            from: 'bottom',
                            align: "center"
                        }
                    });
                },
                error: function(data){
                    notif.close();
                    notif = $.notify({
                        message: 'Une erreur est survenue !'
                    },{
                        // settings
                        type: 'danger',
                        z_index: 2000,
                        placement: {
                            from: 'bottom',
                            align: "center"
                        }
                    });
                }
            });
        }
    });

    $("body").on("click","#btn-modif-pass",function(){
        if($(".input_pass.is-invalid").length == 0)
        {
            var notif = $.notify({
                message: 'Envoie en cours...'
            },{
                // settings
                type: 'warning',
                z_index: 2000,
                placement: {
                    from: 'bottom',
                    align: "center"
                }
            });
            tab_infos = {};
            tab_infos.token = Cookies.get("token");
            tab_infos.pass = $(".input_pass").first().val();
            $.ajax({
                url: url+"user/pass",
                type: 'PUT',
                data: tab_infos,
                success: function(data) {

                    $.each(tab_infos,function(index){
                        $("#"+index+"_info").attr("data-origin",this.toString());
                    });

                    notif.close();
                    notif = $.notify({
                        message: 'Modification effectué !'
                    },{
                        // settings
                        type: 'success',
                        z_index: 2000,
                        placement: {
                            from: 'bottom',
                            align: "center"
                        }
                    });
                },
                error: function(data){
                    notif.close();
                    notif = $.notify({
                        message: 'Une erreur est survenue !'
                    },{
                        // settings
                        type: 'danger',
                        z_index: 2000,
                        placement: {
                            from: 'bottom',
                            align: "center"
                        }
                    });
                }
            });
        }
    });
});