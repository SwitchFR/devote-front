$(document).ready(function(){
    $.get(url+"framework/best",function(data){
        var infos_framework = data.result;
        $("#container_best_framework img").attr("href",infos_framework.image);
        $("#txt_base_framework").text(infos_framework.description);
        $(".cont").attr("data-framework",data.result.id);
        if(Cookies.get("token") == undefined)
        {
            $("#review_lbl").text("Connectez vous pour laisser un avis !");
            $(".star").removeClass("star-activate");
        }
        else
        {
            $.get(url+"user/votes?framework="+data.result.id,{token:Cookies.get("token")},function(data){
                if(data.result != undefined)
                {
                    $("#review_lbl").text("Vous avez déjà noté ce framework !");
                    $('[data-index="'+data.result.note+'"]').click();
                    $(".review").val(data.result.message).attr("readonly",true);
                    $(".btn-vote").attr("disabled",true);
                    $("input.star").attr("disabled",true)
                }
            });
        }
    });

    $("body").on("click",".animated-button1",function(){
        if(Cookies.get("token"))
        {
            window.location = "account.html";
        }
        else
        {
            $("#login").click();
        }
    });
});