$(document).ready(function(){
    $("body").on("click","#btn-contact-send",function(){
        var tab_infos = {};
        $.each($("#contact_zone .form-control"),function(){
            if($(this).val() == "")
            {
                $(this).addClass("is-invalid");
            }
            else
            {
                if($(this).attr("type") == "email" && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(this).val()))
                {
                    $(this).addClass("is-invalid");
                }
                else
                {
                    var id = $(this).attr("id");
                    tab_infos[id.split("_")[1]] = $(this).val(),
                    $(this).removeClass("is-invalid");
                }
            }
        });
        if($(".is-invalid").length == 0)
        {
            var contact_saved = false;
            var notify = $.notify('<strong>Attention</strong> Nous enregistrons votre message...', {
                allow_dismiss: false,
                showProgressbar: true,
                placement: {
                    from: 'top',
                    align: "center"
                }
            });
    
            var interval = setInterval(function(){
                notify.update({'progress': 0})
                if(contact_saved)
                {
                    notify.update({'type': 'success', 'message': 'Votre message est bien enregistré !','progress': 100});
                    clearInterval(interval);
                }
            },1000);
    
            $.post(url+"contact",tab_infos,function(data){
                contact_saved = true;
                $("#contact_zone .form-control").val("");
            }).fail(function(data){
                notify.update({'type': 'danger', 'message': 'Une erreur est survenue !','progress': 100});
                $("#btn-vote").attr("disabled",false);
            });
        }
    });
});